package com.boboysdadda.springaop.service;

import com.boboysdadda.springaop.model.Circle;
import com.boboysdadda.springaop.model.Triangle;

/**
 * Created by james on 4/26/2016.
 */
public class ShapeService {

    private Circle circle;
    private Triangle triangle;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public Triangle getTriangle() {
        return triangle;
    }

    public void setTriangle(Triangle triangle) {
        this.triangle = triangle;
    }
}
