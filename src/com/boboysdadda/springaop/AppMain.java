package com.boboysdadda.springaop;

import com.boboysdadda.springaop.service.ShapeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by james on 4/26/2016.
 */
public class AppMain {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        ShapeService shapeService =  context.getBean("shapeService",ShapeService.class);

        shapeService.getCircle().setName("circley");
        System.out.println(shapeService.getCircle().getName());

    }
}
