package com.boboysdadda.springaop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * Created by james on 4/26/2016.
 */
@Aspect
public class Logging {


    @Before("allCircleMethods()")
    public void loggingAdvice(JoinPoint joinPoint) {
        System.out.println("Advice run. " + joinPoint.getTarget() + " called");
    }

    @Before("args(name)")
    public void stringArgumentMethods(String name){
        System.out.println("Method that takes String arguments has been called. String = " + name );
    }

    @Pointcut("execution(public * get*(..))")
    public void allGetters() {
    }

    @Pointcut("within(com.boboysdadda.springaop.model.Circle)")
    public void allCircleMethods() {
    }


}
