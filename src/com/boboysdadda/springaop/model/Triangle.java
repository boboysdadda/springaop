package com.boboysdadda.springaop.model;

/**
 * Created by james on 4/26/2016.
 */
public class Triangle {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
